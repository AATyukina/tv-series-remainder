import os
import logging

from dotenv import load_dotenv

ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
ENV_FILE_NAME = "../conf.env"

load_dotenv(os.path.join(ROOT_PATH, ENV_FILE_NAME))

TELEGRAM_BOT_TOKEN = os.environ["TELEGRAM_BOT_TOKEN"]

DB_NAME = os.environ["DB_NAME"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_USER = os.environ["DB_USER"]

KINOPOISK_API_TOKEN = os.environ["KINOPOISK_API_TOKEN"]
KINOPOISK_API_HOST = "https://kinopoiskapiunofficial.tech/api/v2.2/"

PATH_TO_LOGS = os.path.join(ROOT_PATH, "logs/current.log")


# shadows name from logging.getLogger
def getLogger(name: str):
    formatter = logging.Formatter('%(levelname)s: %(asctime)s - %(name)s - %(module)s - %(lineno)d - %(message)s')

    # std_output handler
    std_output_handler = logging.StreamHandler()
    std_output_handler.setLevel(logging.DEBUG)
    std_output_handler.setFormatter(formatter)

    # file handler
    file_handler = logging.FileHandler(PATH_TO_LOGS, encoding="utf-8")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(std_output_handler)
    logger.addHandler(file_handler)

    return logger
