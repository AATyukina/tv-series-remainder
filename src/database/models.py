from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class ChatTVShow(Base):
    __tablename__ = "chat_tv_shows"
    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer, nullable=False)
    tv_show_info_id = Column(ForeignKey("tv_shows.id"), nullable=False)     # type: ignore

    __table_args__ = (UniqueConstraint("chat_id", "tv_show_info_id"),)


class TVShow(Base):
    __tablename__ = "tv_shows"
    id = Column(Integer, primary_key=True)
    kp_id = Column(Integer, nullable=False)
    episodes_num = Column(Integer)
    name = Column(String(30), nullable=False, unique=True)
