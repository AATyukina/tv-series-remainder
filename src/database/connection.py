from urllib.parse import quote

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from src.definitions import DB_NAME, DB_USER, DB_PASSWORD


def get_db_url() -> str:
    return f"postgresql+psycopg2://{DB_USER}:{quote(DB_PASSWORD)}@localhost/{DB_NAME}"


def connect():
    engine = create_engine(get_db_url())
    try:
        engine.connect()
    except OperationalError:
        # add log here
        raise
    else:
        return (engine,)


if __name__ == "__main__":
    print(connect())
