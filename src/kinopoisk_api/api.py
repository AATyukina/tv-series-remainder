import json
from json import JSONDecodeError
from typing import Optional
from typing import Union, Dict
from urllib.parse import urljoin

from requests import Session, PreparedRequest
from requests.exceptions import RequestException

from src.definitions import KINOPOISK_API_HOST, KINOPOISK_API_TOKEN, getLogger

logger = getLogger(__name__)


class KinopoiskAPI:
    def __init__(self):
        self._default_method = "GET"
        self._default_headers = {
            "Content-type": "application/json",
            "X-API-KEY": self._get_token_value()
        }

    def get_serials_info_by_name(self, name: str):
        # don't add slash at the beginning, this breaks the url
        url = "films"
        params = {
            "type": "TV_SERIES",
            "keyword": name
        }
        return self._get_requested_information(url=url, params=params)

    def get_serial_info_by_id(self, kp_id: Union[int, str]):
        # don't add slash at the beginning, this breaks the url
        url = f"films/{kp_id}/seasons"
        return self._get_requested_information(url=url)

    def _get_requested_information(self, *, url: str, method: str = "", headers=None, params=None):
        headers = headers or self._default_headers
        method = method or self._default_method
        request = self._make_request_object(url=url, method=method, headers=headers, params=params)
        try:
            response = self._make_request_call(request)
            content = json.loads(response.content)
            return content
        except (CustomRequestException, JSONDecodeError) as ext:
            logger.error(f"Exception happened during requesting or processing data from "
                         f"{url = } with {headers = }; {params = }; {ext = }.")
            raise ext

    @staticmethod
    def _make_request_call(request: PreparedRequest):
        sess = Session()
        try:
            response = sess.send(request)
            if response.status_code >= 300:
                raise RequestException(f"Status code {response.status_code}")
            return response
        except RequestException as ext:
            logger.error("Error during requesting Kinopoisk API.")
            raise CustomRequestException(str(ext))

    def _make_request_object(self, *, url: str, method: str = "", headers=None, params=None) -> PreparedRequest:
        if headers is None:
            headers = {}
        params_dict = {
            "method": method or self._default_method,
            "host": self._get_host(),
            "url": url,
            "headers": headers or self._default_headers,
            "params": params or {}
        }
        request = RequestFormatter(**params_dict).create_request()  # type: ignore
        return request

    @staticmethod
    def _get_host() -> str:
        return KINOPOISK_API_HOST

    @staticmethod
    def _get_token_value():
        return KINOPOISK_API_TOKEN


class RequestFormatter:
    def __init__(self, method: str, host: str, url: str, headers: dict, params: Optional[Dict] = None):
        self.method = method
        self.host = host
        self.url = url
        self.headers = headers
        self.params = params

    def create_request(self) -> PreparedRequest:
        prep_request = PreparedRequest()
        self.add_method(prep_request)
        self.add_url(prep_request)
        self.add_headers(prep_request)

        return prep_request

    def add_method(self, prep_request: PreparedRequest):
        prep_request.prepare_method(self.method)

    def add_url(self, prep_request: PreparedRequest):
        prep_request.prepare_url(self.full_url, params=self.params)

    def add_headers(self, prep_request: PreparedRequest):
        prep_request.prepare_headers(self.headers)

    @property
    def full_url(self) -> str:
        return urljoin(self.host, self.url)


class CustomRequestException(RequestException):
    pass
