pyTelegramBotAPI==4.5.0
SQLAlchemy==1.4.36
psycopg2==2.9.3
alembic==1.7.7
python-dotenv==0.20.0
requests==2.27.1
